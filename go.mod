module gitlab.com/ksahli/examples-bolt

go 1.13

require (
	github.com/etcd-io/bbolt v1.3.3
	golang.org/x/sys v0.0.0-20200124204421-9fbb57f87de9 // indirect
)
