package internal

import (
	bolt "github.com/etcd-io/bbolt"

	"log"
)

var bucket = []byte("toggles")

func Initialize() error {
	log.Println("initializing bucket ...")
	err := db.Update(func(tx *bolt.Tx) error {
		if _, err := tx.CreateBucketIfNotExists(bucket); err != nil {
			return err
		}
		log.Println("done")
		return nil
	})
	return err
}
