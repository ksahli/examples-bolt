package internal

import (
	bolt "github.com/etcd-io/bbolt"

	"log"
)

var db *bolt.DB

func Connect() {
	var err error
	if db, err = bolt.Open("/tmp/toggles", 0600, nil); err != nil {
		log.Fatal(err)
	}
	log.Println("connected")
}

func Disconnect() {
	if err := db.Close(); err != nil {
		log.Fatal(err)
	}
	log.Println("disconnected")
}
