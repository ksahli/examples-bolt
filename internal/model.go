package internal

import (
	"fmt"
)

type Toggle struct {
	Name    string
	Enabled bool
}

func (tgl Toggle) String() string {
	status := "disabled"
	if tgl.Enabled {
		status = "enabled"
	}
	return fmt.Sprintf("%10s:%s", tgl.Name, status)
}

func (tgl *Toggle) Enable() {
	tgl.Enabled = true
}

func (tgl *Toggle) Disable() {
	tgl.Enabled = false
}
