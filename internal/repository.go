package internal

import (
	bolt "github.com/etcd-io/bbolt"

	"errors"
	"log"
)

func Save(name string, enabled bool) error {
	log.Printf("saving toggle %s ...", name)
	return db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket(bucket)
		if b == nil {
			return errors.New("bukcet does not exist")
		}
		key := []byte(tgl.Name)
		return b.Put(key, []byte(0))
	})
}

func List() ([]Toggle, error) {
	log.Println("fetching toggles ...")
	toggles := make([]Toggle)
	err := db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket(bucket)
		if b == nil {
			return errors.New("bucket does not exist")
		}
		b.ForEach(func(key, value []byte) error {
			name := string(key)
			if len(value) == 0 {
				return errors.New("no data")
			}
			status := value[0] == 0
			toggles = append(toggles, Toggle{
				Name:    name,
				Enabled: status,
			})
			return nil
		})
		return nil
	})
	return toggles, err
}

func Fetch(name string) (*Toggle, error) {
	log.Printf("fetching toggle %s ...", name)
	toggle := new(Toggle)
	err := db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket(bucket)
		if b == nil {
			return errors.New("bukcet does not exist")
		}
		data := b.Get([]byte(name))
		if len(data) == 0 {
			return errors.New("no data")
		}
		status = data[0] != byte(0)
		toggle := Toggle{
			Name:    name,
			Enabled: status,
		}
		return nil
	})
	return toggle, err
}

func Remove(name string) error {
	log.Printf("removing toggle %s ...", name)
	return errors.New("not implemented")
}
