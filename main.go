package main

import (
	"gitlab.com/ksahli/examples-bolt/internal"

	"errors"
	"fmt"
	"log"
	"os"
)

func execute(command string, name string) error {
	switch command {

	case "create":
		return internal.Create(name)

	case "fetch":
		if name == "all" {
			toggles, err := internal.List()
			if err != nil {
				return err
			}
			print(toggles)
		} else {
			_, err := internal.Fetch(name)
			if err != nil {
				return err
			}
		}
		return nil

	case "enable":
		return internal.Update(name, true)

	case "disable":
		return internal.Update(name, false)

	case "remove":
		return internal.Remove(name)

	default:
		return errors.New("unknown command")
	}
}

func main() {
	internal.Connect()
	defer internal.Disconnect()

	if err := internal.Initialize(); err != nil {
		log.Panic(err)
	}

	command, name := os.Args[1], os.Args[2]
	if err := execute(command, name); err != nil {
		log.Panic(err)
	}
}

func print(toggles map[string]bool) {
	for name, status := range toggles {
		fmt.Printf("%10s : %t \n", name, status)
	}
}
